﻿using System;
using System.Diagnostics;
using System.Linq;

namespace Homework28
{
    class Program
    {
        static void Main(string[] args)
        {
            int[][] arrays = new[]
            {
                Enumerable.Repeat(1,    100_000).ToArray(),
                Enumerable.Repeat(1,  1_000_000).ToArray(),
                Enumerable.Repeat(1, 10_000_000).ToArray(),
            };

            foreach (int[] array in arrays)
            {
                var methods = new Func<int[], int>[] {
                    ArraySummator.Sum,
                    ArraySummator.SumParallel,
                    ArraySummator.SumLinqParallel,
                };

                foreach (var method in methods)
                {
                    var elapsed = Time(() => { method(array); });
                    var @string = string.Format(
                        "Array length: {0,10:n0} | Method: {1,-15} | elapsed: {2,5:n1}",
                        array.Length,
                        method.Method.Name,
                        elapsed
                    );
                    Console.WriteLine(@string);
                }
            }

            Console.ReadKey();
        }

        static double Time(Action a)
        {
            var sw = Stopwatch.StartNew();
            a();
            return sw.Elapsed.TotalMilliseconds;
        }
    }
}
