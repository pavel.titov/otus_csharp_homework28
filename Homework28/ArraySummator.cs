﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Homework28
{
    public static class ArraySummator
    {
        public static int Sum(int[] array)
        {
            int sum = 0;
            foreach (int i in array)
            {
                sum += i;
            }
            return sum;
        }

        public static int SumParallel(int[] array)
        {
            int sum = 0;

            var threads = new List<Thread>();
            var nThreads = Environment.ProcessorCount;
            int chunkSize = (int)Math.Ceiling((double)array.Length / nThreads);

            for (int i = 0; i < nThreads; ++i)
            {
                int from = i * chunkSize;
                int to = Math.Min((i + 1) * chunkSize, array.Length);
                if (from > array.Length || from == to)
                {
                    break;
                }

                var thread = new Thread(() =>
                {
                    var chunkSum = Sum(array[from..to]);
                    Interlocked.Add(ref sum, chunkSum);
                });

                threads.Add(thread);

                thread.Start();
            }

            foreach(var t in threads)
            {
                t.Join();
            }

            return sum;
        }

        public static int SumLinqParallel(int[] array)
        {
            return array.AsParallel().Sum();
        }
    }
}
